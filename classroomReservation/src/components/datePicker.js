import React, { useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicker = React.memo(({ type, buttonTitle, dateKey, setSchedule }) => {
  const [datePickerVisible, setDatePickerVisible] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisible(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisible(false);
  };

  const handleConfirm = (date) => {
    if (type === "time") {
      const hour = date.getHours();
      const minute = date.getMinutes();
      const formattedTime = `${hour}:${minute}`;
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedTime,
      }));
    } else {
        const formattedDate = date.toISOString().split('T')[0];
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedDate,
      }));
    }
    hideDatePicker();
  };

  return (
    <View>
      <Button onPress={showDatePicker} color="black" title={buttonTitle} />
      <DateTimePickerModal
        isVisible={datePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        pickerComponentStyleIOS={{ backgroundColor: "#fff" }}
        textColor="#000"
      />
    </View>
  );
});

export default DateTimePicker;
